package com.teknei.bid.command.impl.biometric;

import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.Status;
import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieRegProcStatus;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidRegProcStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class StatusCommand implements Command {

    @Autowired
    private BidRegProcStatusRepository bidRegProcStatusRepository;
    @Autowired
    private BidCurpRepository bidCurpRepository;

    private static final Logger log = LoggerFactory.getLogger(StatusCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) 
    {
         String username = request.getUsername();
        if(username != null && !username.trim().isEmpty()) {
            username = username.length() < 7 ? username : username.substring(0,7);
        } else {
            username = "system";
        }
    	log.info("INFO: StatusCommand - execute:  "+ request.getId()+" : "+ username+" : "+request.getRequestStatus().getValue());
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        Long id = request.getId();
        BidClieCurp bidClieCurp = bidCurpRepository.findTopByIdClie(id);

        BidClieRegProcStatus bidClieRegProcStatus = new BidClieRegProcStatus();
        bidClieRegProcStatus.setCurp(bidClieCurp.getCurp());
        bidClieRegProcStatus.setFchCrea(new Timestamp(System.currentTimeMillis()));
        bidClieRegProcStatus.setIdClie(id);
        bidClieRegProcStatus.setIdEsta(1);
        bidClieRegProcStatus.setIdTipo(3);
        bidClieRegProcStatus.setUsrCrea(username);
        bidClieRegProcStatus.setUsrOpeCrea(username);
        bidClieRegProcStatus.setIdStatus((long) request.getRequestStatus().getValue());
        try {
            bidRegProcStatusRepository.save(bidClieRegProcStatus);
            response.setStatus(Status.ADDRESS_DB_STATUS_OK);
        } catch (Exception e) {
            log.error("Error in save regProcStatus with message: {}", e.getMessage());
            response.setStatus(Status.ADDRESS_DB_STATUS_ERROR);
        }
        return response;
    }
}