package com.teknei.bid.command.impl.biometric;

import com.teknei.bid.command.*;
import com.teknei.bid.common.utils.LogUtil;
import com.teknei.bid.util.biom.MBSSHandler;
import com.teknei.bid.util.biom.MBSSHelperSingleton;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

@Component
public class ParseBiometricCommand implements Command {

    @Autowired
    private MBSSHandler mbssHandler;
    private static final Logger log = LoggerFactory.getLogger(ParseBiometricCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) {
    	log.info("INFO: ParseBiometricCommand.execute:  Id:"+ request.getId()+", ScanId:"+request.getScanId()+", DocumentId:"+request.getDocumentId()+", RequestType:"+request.getRequestType());
        CommandResponse response = new CommandResponse();
        response.setId(request.getId());
        response.setScanId(request.getScanId());
        response.setDocumentId(request.getDocumentId());
        JSONObject jsonObject = new JSONObject(request.getData());
        log.info("INFO: "+LogUtil.logJsonObject(jsonObject));
        ResponseEntity<String> returnResponse = null;
        String facial = jsonObject.optString("facial", null);
        if (facial != null) 
        {
        	log.info("INFO: Facial:"+LogUtil.cutString(facial));
            MBSSHelperSingleton.getInstance().addFace(String.valueOf(request.getId()), facial);
        }
        if (request.getRequestType().equals(RequestType.BIOM_SLAPS_REQUEST)) 
        {
            String ls = jsonObject.optString("ls", null);
            String rs = jsonObject.optString("rs", null);
            String ts = jsonObject.optString("ts", null);
            String contentType = jsonObject.optString("contentType", "wsq");
            if (contentType.contains("/")) {
                String[] contents = contentType.split("\\/");
                contentType = contents[contents.length - 1];
            }
            MBSSHelperSingleton.getInstance().addSlaps(ls, rs, ts, String.valueOf(request.getId()), contentType);
            returnResponse = registerMbss(request.getIneType(),String.valueOf(request.getId()), 2);
            log.info("INFO: ParseBiometricCommand 3.1: returnResponse:"+returnResponse);
        }
        else 
        {
        	log.info("INFO: ParseBiometricCommand 4");
            String ll = jsonObject.optString("ll", null);
            String lr = jsonObject.optString("lr", null);
            String lm = jsonObject.optString("lm", null);
            String li = jsonObject.optString("li", null);
            String lt = jsonObject.optString("lt", null);
            String rl = jsonObject.optString("rl", null);
            String rr = jsonObject.optString("rr", null);
            String rm = jsonObject.optString("rm", null);
            String ri = jsonObject.optString("ri", null);
            String rt = jsonObject.optString("rt", null);
            String contentType = jsonObject.optString("contentType", "jpeg");
            if (contentType.contains("/")) 
            {
                String[] contents = contentType.split("\\/");
                contentType = contents[contents.length - 1];
            }
            MBSSHelperSingleton.getInstance().addFingers(String.valueOf(request.getId()), ll, lr, lm, li, lt, rl, rr, rm, ri, rt, contentType);
            returnResponse = registerMbss(request.getIneType(),String.valueOf(request.getId()), 1);
            log.info("INFO: ParseBiometricCommand 4.1 returnResponse:"+returnResponse);
        }
        log.info("INFO: ParseBiometricCommand.execute: Return response from registerMbss: {}", returnResponse);
        switch (returnResponse.getStatusCode().value()) {
            case 409:
                response.setStatus(Status.BIOM_DUPLICATE_RECORD_ERROR);
                break;
            case 412:
                response.setStatus(Status.BIOM_DUPLICATE_FINGER);
                break;
            case 500:
                response.setStatus(Status.BIOM_ERROR);
                break;
            case 200:
                response.setStatus(Status.BIOM_OK);
                break;
        }
        log.info("INFO: ParseBiometricCommand.execute:  "+ response);
        return response;
    }


    private ResponseEntity<String> registerMbss(Integer ineType, String operationId, int type) {
    	log.info("INFO: ParseBiometricCommand.registerMbss:  operationId:"+ operationId + "  type:"+type);
        int code = 200;
        try {
            ResponseEntity<String> responseBiom = null;
            if (type == 1) 
            {
                if (MBSSHelperSingleton.getInstance().getFace(operationId) != null) 
                {
                    responseBiom = mbssHandler.registerFingersAndFace(operationId);
                }
                else 
                {
                    responseBiom = mbssHandler.registerFingers(operationId,ineType);
                }
            }
            else 
            {
                if (MBSSHelperSingleton.getInstance().getFace(operationId) != null) {
                    responseBiom = mbssHandler.registerSlapsAndFace(operationId);
                } else {
                    responseBiom = mbssHandler.registerSlaps(operationId);
                }
            }
            code = responseBiom.getStatusCode().value();
            String body = responseBiom.getBody();
            if (code != 200) {
                switch (code) {
                    case 409:
                        return new ResponseEntity<>("", HttpStatus.CONFLICT);
                    case 412:
                        return new ResponseEntity<>("", HttpStatus.PRECONDITION_FAILED);
                    case 500:
                        return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
                    default:
                        log.warn("WARN: ParseBiometricCommand.registerMbss:Code not recognized: {}", code);
                        return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                return new ResponseEntity<>("", HttpStatus.OK);
            }
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().value() == 409) {
                return new ResponseEntity<>("", HttpStatus.CONFLICT);
            } else if (e.getStatusCode().value() == 412) {
                return new ResponseEntity<>("", HttpStatus.PRECONDITION_FAILED);
            }
            return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (Exception e) {
            log.error("ERROR: ParseBiometricCommand.registerMbss: Error: {}", e.getMessage());
            return new ResponseEntity<>("", HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
