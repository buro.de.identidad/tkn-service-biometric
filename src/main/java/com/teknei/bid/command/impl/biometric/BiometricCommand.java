package com.teknei.bid.command.impl.biometric;

import com.teknei.bid.command.*;
import com.teknei.bid.persistence.entities.BidClieRegEsta;
import com.teknei.bid.persistence.entities.BidClieTas;
import com.teknei.bid.persistence.entities.BidEstaProc;
import com.teknei.bid.persistence.entities.BidScan;
import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidClieRegEstaRepository;
import com.teknei.bid.persistence.repository.BidEstaProcRepository;
import com.teknei.bid.persistence.repository.BidScanRepository;
import com.teknei.bid.persistence.repository.BidTasRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class BiometricCommand implements Command {

    @Autowired
    @Qualifier(value = "parseBiometricCommand")
    private Command parseBiometricCommand;
    @Autowired
    @Qualifier(value = "storeTasBiometricCommand")
    private Command storeTasBiometricCommand;
    @Autowired
    @Qualifier(value = "persistBiometricCommand")
    private Command persistBiometricCommand;
    @Autowired
    private BidTasRepository bidTasRepository;
    @Autowired
    private BidScanRepository bidScanRepository;
    @Autowired
    @Qualifier(value = "statusCommand")
    private Command statusCommand;
    @Autowired
    private BidEstaProcRepository bidEstaProcRepository;
    @Autowired
    private BidClieRegEstaRepository bidClieRegEstaRepository;
    
    private static final String ESTA_PROC = "CAP-DAC";
    private static final Logger log = LoggerFactory.getLogger(BiometricCommand.class);

    @Override
    public CommandResponse execute(CommandRequest request) 
    {
        if(request.getUsername() != null && !request.getUsername().trim().isEmpty()) {
            request.setUsername(request.getUsername().length() < 7 ? request.getUsername() : request.getUsername().substring(0,7));
        } else {
            request.setUsername("system");
        }
    	log.info("INFO BiometricCommand.execute:  "+request.getCurp());
        String scanId = findScanId(request.getId());
        String documentId = findDocumentId(request.getId());
        request.setScanId(scanId);
        request.setDocumentId(documentId);
        CommandResponse responseBiometric = parseBiometricCommand.execute(request);
        log.info("INFO BiometricCommand.execute: isForzeAddFinger:"+request.isForzeAddFinger());
        if(request.isForzeAddFinger())
        {
	            saveStatus(request.getId(), Status.BIOM_OK, request.getUsername());
	            if (request.getRequestType().equals(RequestType.BIOM_FINGERS_REQUEST)) 
	            {
	            	log.info("INFO BiometricCommand.execute: Save TAS.. ");
	                CommandRequest storeTasRequest = new CommandRequest();
	                storeTasRequest.setScanId(request.getScanId());
	                storeTasRequest.setDocumentId(request.getDocumentId());
	                storeTasRequest.setId(request.getId());
	                storeTasRequest.setData(request.getData());
	                storeTasRequest.setRequestType(request.getRequestType());
	                CommandResponse storeTasResponse = storeTasBiometricCommand.execute(storeTasRequest);
	            	log.info("INFO BiometricCommand.execute: Save TAS...  SUCSESS!!");
	                if (!storeTasResponse.getStatus().equals(Status.BIOM_TAS_OK) && request.getIneType() == null) {
	                    storeTasResponse.setDesc(String.valueOf(storeTasResponse.getStatus().getValue()));
	                    storeTasResponse.setStatus(Status.BIOM_ERROR);
	                    saveStatus(request.getId(), Status.BIOM_ERROR, request.getUsername());
	                    return storeTasResponse;
	                }
	            }
	            log.info("INFO BiometricCommand.execute:Save Status.. Status.BIOM_MBSS_OK");
	            saveStatus(request.getId(), Status.BIOM_MBSS_OK, request.getUsername());
	            CommandRequest persistRequest = new CommandRequest();
	            persistRequest.setData(request.getData());
	            persistRequest.setId(request.getId());
	            persistRequest.setUsername(request.getUsername());
	            persistRequest.setDocumentId(request.getDocumentId());
	            persistRequest.setScanId(request.getScanId());
	            persistRequest.setRequestType(request.getRequestType());
	            CommandResponse dbResponse = persistBiometricCommand.execute(persistRequest);
	            log.info("INFO BiometricCommand.execute:Save Status.. SUCSESS!!");
	            if (!dbResponse.getStatus().equals(Status.BIOM_DB_OK)) 
	            {
	                dbResponse.setDesc(String.valueOf(dbResponse.getStatus().getValue()));
	                dbResponse.setStatus(Status.BIOM_ERROR);
	                saveStatus(request.getId(), Status.BIOM_DB_ERROR, request.getUsername());
	                return dbResponse;
	            }
	            dbResponse.setDesc(String.valueOf(dbResponse.getStatus().getValue()));
	            dbResponse.setStatus(Status.BIOM_OK);
	            saveStatus(request.getId(), Status.BIOM_DB_OK, request.getUsername());
	            saveStatus(request.getId(), Status.BIOM_OK, request.getUsername());
	            updateStatus(request.getId(), request.getUsername());
	            log.info("INFO BiometricCommand.execute: dbResponse:"+ dbResponse);
	            return dbResponse;
        }        
        if (!responseBiometric.getStatus().equals(Status.BIOM_OK)) 
        {
            responseBiometric.setDesc(String.valueOf(responseBiometric.getStatus().getValue()));
            responseBiometric.setStatus(Status.BIOM_ERROR);
            saveStatus(request.getId(), Status.BIOM_ERROR, request.getUsername());
            log.info("INFO BiometricCommand.execute:Return from command responseBiometric: {}", responseBiometric);
            return responseBiometric;
        }
        saveStatus(request.getId(), Status.BIOM_OK, request.getUsername());
        if (request.getRequestType().equals(RequestType.BIOM_FINGERS_REQUEST)) {
            CommandRequest storeTasRequest = new CommandRequest();
            storeTasRequest.setScanId(request.getScanId());
            storeTasRequest.setDocumentId(request.getDocumentId());
            storeTasRequest.setId(request.getId());
            storeTasRequest.setData(request.getData());
            storeTasRequest.setRequestType(request.getRequestType());
            
				CommandResponse storeTasResponse = storeTasBiometricCommand.execute(storeTasRequest);
				if (!storeTasResponse.getStatus().equals(Status.BIOM_TAS_OK) && request.getIneType() == null) {
					storeTasResponse.setDesc(String.valueOf(storeTasResponse.getStatus().getValue()));
					storeTasResponse.setStatus(Status.BIOM_ERROR);
					saveStatus(request.getId(), Status.BIOM_ERROR, request.getUsername());
					return storeTasResponse;
				}else{
                    	log.error("ERROR AL CONECTAR AL TAS ");
                }
			
        }
        saveStatus(request.getId(), Status.BIOM_MBSS_OK, request.getUsername());
        CommandRequest persistRequest = new CommandRequest();
        persistRequest.setData(request.getData());
        persistRequest.setId(request.getId());
        persistRequest.setUsername(request.getUsername());
        persistRequest.setDocumentId(request.getDocumentId());
        persistRequest.setScanId(request.getScanId());
        persistRequest.setRequestType(request.getRequestType());
        CommandResponse dbResponse = persistBiometricCommand.execute(persistRequest);
        if (!dbResponse.getStatus().equals(Status.BIOM_DB_OK)) {
            dbResponse.setDesc(String.valueOf(dbResponse.getStatus().getValue()));
            dbResponse.setStatus(Status.BIOM_ERROR);
            saveStatus(request.getId(), Status.BIOM_DB_ERROR, request.getUsername());
            return dbResponse;
        }
        dbResponse.setDesc(String.valueOf(dbResponse.getStatus().getValue()));
        dbResponse.setStatus(Status.BIOM_OK);
        saveStatus(request.getId(), Status.BIOM_DB_OK, request.getUsername());
        saveStatus(request.getId(), Status.BIOM_OK, request.getUsername());
        updateStatus(request.getId(), request.getUsername());
        log.info("INFO BiometricCommand.execute:  dbResponse :"+ dbResponse);
        return dbResponse;
    }

    /**
     * Persists the current status for the main process
     *
     * @param id
     * @param status
     * @return
     */
    private CommandResponse saveStatus(Long id, Status status, String username) {
        if(username != null && !username.trim().isEmpty()) {
            username = username.length() < 7 ? username : username.substring(0,7);
        } else {
            username = "system";
        }
    	log.info("INFO: BiometricCommand.saveStatus:  id:"+ id+", username: "+ username);
    	CommandRequest request = new CommandRequest();
        request.setId(id);
        request.setUsername(username);
        request.setRequestStatus(status);
        CommandResponse response = statusCommand.execute(request);
        return response;
    }

    private String findScanId(Long id) {
    	log.info("INFO: BiometricCommand.findScanId:  id:"+ id );
        try {
            BidScan responseScan = bidScanRepository.findByIdRegi(id);
            if (responseScan == null) {
                return null;
            }
            return responseScan.getScanId();
        } catch (Exception e) {
            log.error("Error find scanId: {}", e.getMessage());
        }
        return null;
    }

    private String findDocumentId(Long id) 
    {
    	log.info("INFO: BiometricCommand.findDocumentId: id"+ id );
        try {
            BidClieTas responseScan = bidTasRepository.findByIdClie(id);
            if (responseScan == null) {
                return null;
            }
            return responseScan.getIdTas();
        } catch (Exception e) {
            log.error("Error find scanId: {}", e.getMessage());
        }
        return null;
    }

    private void updateStatus(Long idClient, String username) {
        if(username != null && !username.trim().isEmpty()) {
            username = username.length() < 7 ? username : username.substring(0,7);
        } else {
            username = "system";
        }

        log.info("Method updateStatus - Username: {}", username);
    	log.info("INFO: BiometricCommand.updateStatus:  idClient:"+ idClient + "   username:"+username);
        try {
            BidEstaProc estaProc = bidEstaProcRepository.findTopByCodEstaProcAndIdEsta(ESTA_PROC, 1);
            BidClieRegEsta regEsta = bidClieRegEstaRepository.findByIdClieAndIdEstaProc(idClient, estaProc.getIdEstaProc());
            if (regEsta == null) {
                log.warn("Status for process: {} found null", idClient);
            }
            regEsta.setEstaConf(true);
            regEsta.setFchModi(new Timestamp(System.currentTimeMillis()));
            regEsta.setUsrModi(username);
            regEsta.setUsrOpeModi(username);
            bidClieRegEstaRepository.save(regEsta);
        } catch (Exception e) {
            log.error("ERROR: BiometricCommand.updateStatus: Error finding status of process for customer: {} with message: {}", idClient, e.getMessage());
        }
    }
}
