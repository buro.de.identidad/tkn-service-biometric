package com.teknei.bid.command;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
public class CommandResponse implements Serializable{

    private Status status;
    private String desc;
    private long id;
    private String scanId;
    private String documentId;
    private String curp;
	@Override
	public String toString() {
		return "CommandResponse [status=" + status + ", desc=" + desc + ", id=" + id + ", scanId=" + scanId
				+ ", documentId=" + documentId + ", curp=" + curp + "]";
	}
    

}