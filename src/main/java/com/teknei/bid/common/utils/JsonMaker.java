package com.teknei.bid.common.utils;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.teknei.bid.controller.rest.BiometricController;

/**
 * Clase utilitaria para el logeo de json extensos.
 * 
 * @author AJGD
 */
public abstract class JsonMaker {
	
	private static final Logger log = LoggerFactory.getLogger(BiometricController.class);

	/**
	 * Create fingersJson
	 * 
	 * @param jsonObject
	 * @return
	 */
	public static JSONObject CreatefingersJson(JSONObject jsonObject) {
		JSONObject fingersJson = new JSONObject();
		fingersJson.put("li", jsonObject.optString("li", null));
		fingersJson.put("lt", jsonObject.optString("rt", null));
		fingersJson.put("lm", jsonObject.optString("lm", null));
		fingersJson.put("lr", jsonObject.optString("lr", null));
		fingersJson.put("ll", jsonObject.optString("ll", null));
		fingersJson.put("ri", jsonObject.optString("ri", null));
		fingersJson.put("rt", jsonObject.optString("rt", null));
		fingersJson.put("rm", jsonObject.optString("rm", null));
		fingersJson.put("rr", jsonObject.optString("rr", null));
		fingersJson.put("rl", jsonObject.optString("rl", null));
		return fingersJson;
	}
	
	/**
	 * Create requestFingersJson
	 * @param jsonObject
	 * @param id
	 * @param imageType
	 * @return
	 */
	public static JSONObject requestFingersJson(JSONObject jsonObject,String id,String imageType) {
		JSONObject requestJson = new JSONObject();
	    requestJson.put("id", id);
	    requestJson.put("fingers", JsonMaker.CreatefingersJson(jsonObject));
	    requestJson.put("imageType", imageType);
	    return requestJson;
	    
	}

}