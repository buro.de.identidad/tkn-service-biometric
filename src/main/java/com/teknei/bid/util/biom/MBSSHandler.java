package com.teknei.bid.util.biom;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;  
import com.teknei.bid.persistence.entities.BidClieCurp; 
import com.teknei.bid.persistence.repository.BidCurpRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class MBSSHandler { 
	
	@Value("${biometric.engine}")
	private String engine;
	private static final String KARALUNDI = "karalundi";  

    @Autowired
    private DiscoveryClient discoveryClient;

    private String mbssBaseUrl = "BID-SERV-API-MBSS";// = "http://192.168.1.101:18090";
    @Autowired
    private BidCurpRepository bidCurpRepository;
    
    public static String findUrl(String serviceName, String toAppend, DiscoveryClient discoveryClient) {
    	log.info("INFO: MBSSHandler.findUrl");
    	List<ServiceInstance> instanceList = discoveryClient.getInstances(serviceName);
        if (instanceList == null) {
            return null;
        }
        ServiceInstance first = instanceList.get(0);
        String foudUri = new StringBuilder(first.getUri().toString()).append("/").append(toAppend).toString();
        return foudUri;
    }

    private static final Logger log = LoggerFactory.getLogger(MBSSHandler.class);

    /**
     * Registers the fingers into Biometric DB
     *
     * @param leftIndex  - Base64 String
     * @param rightIndex - Base 64 String
     * @param id         - the id of the request to be saved
     * @return - 200 if success, 400 if fingers not well formed, 409 if fingers already in biometric db, 500 if error in biometric system
     */
    public ResponseEntity<String> registerFingers(String leftLitle, String leftRing, String leftMiddle, String leftIndex, String leftThumb, String rightLittle, String rightRing, String rightMiddle, String rightIndex, String rightThumb, String id,Integer ineType) {
    	log.info("INFO: MBSSHandler.registerFingers");
    	JSONObject requestJson = new JSONObject();
        String nStr = null;
        requestJson.put("fingers", getFingers(leftLitle, leftRing, leftMiddle, leftIndex, leftThumb, rightLittle, rightRing, rightMiddle, rightIndex, rightThumb));
        requestJson.put("imageType", "wsq");
        requestJson.put("slaps", nStr);
        requestJson.put("id", id);
        requestJson.put("type", 1);
        requestJson.put("ineType", ineType);
        return executeRequest(requestJson.toString());
    }

    /**
     * Registers the fingers in the biometric system
     *
     * @param id
     * @return
     */
    public ResponseEntity<String> registerFingers(String id,Integer ineType) {
    	log.info("INFO: MBSSHandler.registerFingers");
    	JSONObject fingers = MBSSHelperSingleton.getInstance().getFingers(id);
        String ll = fingers.optString("ll", null);
        String lr = fingers.optString("lr", null);
        String lm = fingers.optString("lm", null);
        String li = fingers.optString("li", null);
        String lt = fingers.optString("lt", null);
        String rl = fingers.optString("rl", null);
        String rr = fingers.optString("rr", null);
        String rm = fingers.optString("rm", null);
        String ri = fingers.optString("ri", null);
        String rt = fingers.optString("rt", null);
        return registerFingers(ll, lr, lm, li, lt, rl, rr, rm, ri, rt, id, ineType);
    }

    /**
     * Registers fingers and face in biometric system
     *
     * @param id
     * @return
     */
    public ResponseEntity<String> registerFingersAndFace(String id) {
    	log.info("INFO: MBSSHandler.registerFingersAndFace");
    	String face = MBSSHelperSingleton.getInstance().getFace(id);
        JSONObject fingers = MBSSHelperSingleton.getInstance().getFingers(id);
        String ll = fingers.optString("ll", null);
        String lr = fingers.optString("lr", null);
        String lm = fingers.optString("lm", null);
        String li = fingers.optString("li", null);
        String lt = fingers.optString("lt", null);
        String rl = fingers.optString("rl", null);
        String rr = fingers.optString("rr", null);
        String rm = fingers.optString("rm", null);
        String ri = fingers.optString("ri", null);
        String rt = fingers.optString("rt", null);
        String contentType = fingers.optString("contentType", "jpeg");
        return registerFingersAndFace(ll, lr, lm, li, lt, rl, rr, rm, ri, rt, face, id, contentType);
    }

    /**
     * Registers slaps in the biometric system
     *
     * @param id
     * @return
     */
    public ResponseEntity<String> registerSlaps(String id) {
    	log.info("INFO: MBSSHandler.registerSlaps");
    	JSONObject slaps = MBSSHelperSingleton.getInstance().getSlaps(id);
        String ls = slaps.getString("ls");
        String rs = slaps.getString("rs");
        String ts = slaps.getString("ts");
        return registerSlaps(ls, rs, ts, id);
    }

    /**
     * Registers slaps and face in the biometric system
     *
     * @param id
     * @return
     */
    public ResponseEntity<String> registerSlapsAndFace(String id) {
    	log.info("INFO: MBSSHandler.registerSlapsAndFace");
    	String face = MBSSHelperSingleton.getInstance().getFace(id);
        JSONObject slaps = MBSSHelperSingleton.getInstance().getSlaps(id);
        String ls = slaps.getString("ls");
        String rs = slaps.getString("rs");
        String ts = slaps.getString("ts");
        String contentType = slaps.optString("contentType", "wsq");
        return registerSlapsAndFace(ls, rs, ts, face, id, contentType);
    }

    /**
     * Registers slaps in biometric system
     *
     * @param leftSlap
     * @param rightSlap
     * @param thumbsSlap
     * @param id
     * @return
     */
    public ResponseEntity<String> registerSlaps(String leftSlap, String rightSlap, String thumbsSlap, String id) {
    	log.info("INFO: MBSSHandler.registerSlaps");
    	JSONObject requestJson = new JSONObject();
        String fingers = null;
        requestJson.put("fingers", fingers);
        requestJson.put("id", id);
        requestJson.put("imageType", "wsq");
        JSONObject slaps = new JSONObject();
        slaps.put("ls", leftSlap);
        slaps.put("rs", rightSlap);
        slaps.put("ts", thumbsSlap);
        requestJson.put("slaps", slaps);
        requestJson.put("type", 4);
        return executeRequest(requestJson.toString());
    }

    /**
     * Registers slaps and face in biometric system
     *
     * @param leftSlap
     * @param rightSlap
     * @param thumbsSlap
     * @param face
     * @param id
     * @return
     */
    public ResponseEntity<String> registerSlapsAndFace(String leftSlap, String rightSlap, String thumbsSlap, String face, String id, String contentType) {
    	log.info("INFO: MBSSHandler.registerSlapsAndFace");
    	JSONObject requestJson = new JSONObject();
        requestJson.put("facial", face);
        String fingers = null;
        requestJson.put("fingers", fingers);
        requestJson.put("id", id);
        requestJson.put("imageType", contentType);
        JSONObject slaps = new JSONObject();
        slaps.put("ls", leftSlap);
        slaps.put("rs", rightSlap);
        slaps.put("ts", thumbsSlap);
        requestJson.put("slaps", slaps);
        requestJson.put("type", 6);
        return executeRequest(requestJson.toString());
    }


    /**
     * Registers fingers and face in biometric system
     *
     * @param ll
     * @param lr
     * @param lm
     * @param li
     * @param lt
     * @param rl
     * @param rr
     * @param rm
     * @param ri
     * @param rt
     * @param face
     * @param id
     * @return
     */
    public ResponseEntity<String> registerFingersAndFace(String ll, String lr, String lm, String li, String lt, String rl, String rr, String rm, String ri, String rt, String face, String id, String contentType) {
    	log.info("INFO: MBSSHandler.registerFingersAndFace");
    	JSONObject requestJson = new JSONObject();
        requestJson.put("facial", face);
        JSONObject fingers = new JSONObject();
        String slaps = null;
        fingers.put("li", li);
        fingers.put("ll", ll);
        fingers.put("lm", lm);
        fingers.put("lr", lr);
        fingers.put("lt", lt);
        fingers.put("ri", ri);
        fingers.put("rl", rl);
        fingers.put("rm", rm);
        fingers.put("rr", rr);
        fingers.put("rt", rt);
        requestJson.put("fingers", fingers);
        requestJson.put("id", id);
        requestJson.put("imageType", contentType);
        requestJson.put("slaps", slaps);
        requestJson.put("type", 3);
        return executeRequest(requestJson.toString());
    }

    /**
     * Execute the biometric request to the server
     *
     * @param requestJson
     * @return
     */
    private ResponseEntity<String> executeRequest(String requestJson) {
    	log.info("INFO: MBSSHandler.executeRequest");
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/add";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
        log.info("INFO: MBSSHandler.executeRequest: responseEntity : "+responseEntity);
        return responseEntity;
    }

    public ResponseEntity<String> getHashForCustomer(String id) {
    	log.info("INFO: MBSSHandler.getHashForCustomer");
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/getHash/" + id;
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(uri, String.class);
        log.debug("INFO: MBSSHandler.getHashForCustomer: Response: {}", responseEntity);
        return responseEntity;
    }


    /**
     * Performs a search based on fingers by Id
     *
     * @param leftIndex  - Base64 String
     * @param rightIndex - Base64 String
     * @param id         - the id associated
     * @return - 200 if found, 404 otherwise
     */
    public ResponseEntity<String> searchBasedOnFingersById(String leftLitle, String leftRing, String leftMiddle, String leftIndex, String leftThumb, String rightLittle, String rightRing, String rightMiddle, String rightIndex, String rightThumb, String id, String imageType) {
        log.info("INFO: MBSSHandler.searchBasedOnFingersById ");
    	JSONObject requestJson = new JSONObject();
        requestJson.put("id", id);
        requestJson.put("fingers", getFingers(leftLitle, leftRing, leftMiddle, leftIndex, leftThumb, rightLittle, rightRing, rightMiddle, rightIndex, rightThumb));
        requestJson.put("imageType", imageType);
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/fingersById";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
        try {
//        	log.info("lblancas: "+
//            		"\n uri    ::::::"+uri+
//            		"\n entity:::::::"+entity);
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
//            log.info("responseEntity>>"+responseEntity);
            return responseEntity;
        } catch (HttpClientErrorException e) 
        {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", e.getStatusCode());
            }
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        } catch (HttpServerErrorException hex) {
            if (hex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } else if (hex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error searching fingers by id with message: {}", e.getMessage());
        }
        return null;
    }
    
    //optimizacion de codigo
    public ResponseEntity<String> searchBasedOnFingersById(JSONObject requestJson) {
        log.info("INFO: MBSSHandler.searchBasedOnFingersById ");
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/fingersById";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
        try {
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
            log.info("INFO: MBSSHandler.searchBasedOnFingersById: responseEntity: "+responseEntity);
            return responseEntity;
        } catch (HttpClientErrorException e){
        	log.error("INFO: MBSSHandler.searchBasedOnFingersById: HttpClientErrorException: "+e.getMessage());
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", e.getStatusCode());
            }
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        } catch (HttpServerErrorException hex) {
            log.error("INFO: MBSSHandler.searchBasedOnFingersById: HttpServerErrorException: "+hex.getMessage());
            if (hex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } else if (hex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("ERROR: MBSSHandler.searchBasedOnFingersById: Error searching fingers by id with message: {}", e);
        }
        return null;
    }

    
    /**
     * Search by face and Id
     *
     * @param id
     * @param facial
     * @return
     */
    public ResponseEntity<String> searchBasedOnFaceId(String id, String facial) {
    	log.info("INFO: MBSSHandler.searchBasedOnFaceId");
        JSONObject requestJson = new JSONObject();
        requestJson.put("id", id);
        requestJson.put("facial", facial);
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/facialId";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
        try {
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
            return responseEntity;
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", e.getStatusCode());
            }
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        } catch (HttpServerErrorException hex) {
            if (hex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } else if (hex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("ERROR: MBSSHandler.searchBasedOnFaceId: Error searching fingers by id with message: {}", e.getMessage());
        }
        return null;
    }

    public ResponseEntity<String> searchDetailById(String id) {
    	log.info("INFO: MBSSHandler.searchDetailById");
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/searchDetail/" + id;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try {
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(uri, String.class);
            return responseEntity;
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", e.getStatusCode());
            }
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        } catch (HttpServerErrorException hex) {
            if (hex.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } else if (hex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) {
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("ERROR: MBSSHandler.searchDetailById: Error searching fingers by id with message: {}", e.getMessage());
        }
        return null;
    }

	private ResponseEntity<String> findUserByCurpToVerifyImageKaralundi(String curpIngresado, String facial, String uri, HttpHeaders headers ) {
		// TODO--->
		log.info("          ######### ENTRANDO A PRUEBA FLUJO ALTERNO COMPARACION DE CURP KARALUNDI ######### ");
//  	  1.- buscar en la tabla curp los id asociados
		List<BidClieCurp> usuariosEncontrados = bidCurpRepository.findAllByCurp(curpIngresado);
		if(usuariosEncontrados.size() == 0) {
			return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);//esto esta bienOK } 
		}
//  	  2.- buscar en karalundi los id relacionados al kurp con la foto obtenida.
		// verificando tipo diferente
		List<String> speakers = new ArrayList<>();
		if(usuariosEncontrados.size()==2) {
			if(usuariosEncontrados.get(0).getIdTipo() == usuariosEncontrados.get(1).getIdTipo()){
				log.info("INFO: MBSSHandler.searchBasedOnFace : Usuario de CURP y Tipo Existentes");
	    	}
			// creando lista de speakers
			for (BidClieCurp usr : usuariosEncontrados) {
				speakers.add(usr.getIdClie() + "");			
			}
		}
		// creando peticion
		JSONObject requestJson = new JSONObject();
		requestJson.put("id", usuariosEncontrados.size() > 0 ? usuariosEncontrados.get(0).getIdClie() : 99);
		requestJson.put("facial", facial);
		requestJson.put("speakers", speakers);
		log.info("INFO: " + usuariosEncontrados.size() + " encontrados con el curp :" + curpIngresado);
		RestTemplate restTemplate = new RestTemplate();		
		HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
		try {
			ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
//  	  3.- si existe continuar
			log.info("INFO: StatusCode:" + responseEntity.getStatusCodeValue());
			JSONObject jsonResult = new JSONObject(responseEntity.getBody());
			log.info(this.getClass().getName() + ".findUserByCurpToVerifyImageKaralundi: " + "\n  jsonResult --->:   "
					+ jsonResult.toString());
    		return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);//esto esta bienOK
		} catch (HttpClientErrorException e) {
			log.error("ERROR: StatusCode"+e.getStatusCode());
			if(e.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
				log.error("ERROR: El id de usuario no corresponde con la imagen ingresada:");
				return new ResponseEntity<String>("El id de usuario no corresponde con la imagen ingresada", HttpStatus.OK); // ERROR
			}
			if(e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
				return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);//esto esta bienOK
			}
		} catch (Exception e3) {
			log.error("ERROR: inesperado:", e3);
		}
		// <------
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
    /**
     * Search by face
     * @param id
     * @param facial
     * @return
     */
    public ResponseEntity<String> searchBasedOnFace(String id, String facial) {
    	log.info("INFO: MBSSHandler.searchBasedOnFace");
        JSONObject requestJson = new JSONObject();
        requestJson.put("id", id);
        requestJson.put("facial", facial);
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/facial";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
//        log.info("INFO: MBSSHandler.searchBasedOnFace 2");
        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
        try {
            BidClieCurp   clieCurpRecepcion = (BidClieCurp) bidCurpRepository.findTopByIdClie(new Long(id));
            if(engine.equals(KARALUNDI)) {//TODO FLUJO DEMO --->
        		 ResponseEntity<String> responseEntity = findUserByCurpToVerifyImageKaralundi(clieCurpRecepcion.getCurp(), facial, uri, headers);
        		 return responseEntity;
        	}
//        	log.info("searchBasedOnFace 3");
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
//            log.info("searchBasedOnFace 4:  "+responseEntity);
            JSONObject jsonInput = new JSONObject(responseEntity.getBody());
//            log.info("searchBasedOnFace 4.1:  "+responseEntity);
            String idResultado = jsonInput.optString("id", null); 
//            log.info("clieCurpRecepcion :: " +clieCurpRecepcion.getCurp() + "  ::: " + clieCurpRecepcion.getIdClie() + "  ::: " + clieCurpRecepcion.getIdTipo());
            BidClieCurp clieCurpRespuesta = (BidClieCurp) bidCurpRepository.findTopByIdClie(new Long(idResultado));
//            log.info("clieCurpRespuesta  " +clieCurpRespuesta.getCurp() + "  ::: " + clieCurpRespuesta.getIdClie() + "  ::: " + clieCurpRespuesta.getIdTipo());
//            log.info("searchBasedOnFace 4.3:  "+responseEntity);
            if(clieCurpRecepcion.getCurp().toUpperCase().equals(clieCurpRespuesta.getCurp().toUpperCase()))
            {
            	if(clieCurpRecepcion.getIdTipo() == clieCurpRespuesta.getIdTipo())
            	{
            		log.info("INFO: MBSSHandler.searchBasedOnFace : Usuario de CURP y Tipo Existentes");
            	}
            	else 
            	{
            		log.info("INFO: MBSSHandler.searchBasedOnFace : Usuario de CURP  Existentes , pero con Tipo diferente");
            		return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            	}
            }
            else
            {
            	log.info("INFO: MBSSHandler.searchBasedOnFace : Curp Diferentes");
            }
            return responseEntity;
        } catch (HttpClientErrorException e) {
        	log.info("ERROR: MBSSHandler.searchBasedOnFace: HttpClientErrorException: "+ e.getMessage());
            if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) 
            {
//            	log.info("searchBasedOnFace 5:  Usuario no encontrado");
                return new ResponseEntity<>("Usuario no encontrado", e.getStatusCode());
            }
            return new ResponseEntity<>(e.getMessage(), e.getStatusCode());
        } catch (HttpServerErrorException hex) {
        	log.info("ERROR: MBSSHandler.searchBasedOnFace: HttpServerErrorException: "+ hex.getMessage());
            if (hex.getStatusCode().equals(HttpStatus.NOT_FOUND)) 
            {
//            	log.info("searchBasedOnFace 6:  Usuario no encontrado");
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } else if (hex.getStatusCode().equals(HttpStatus.INTERNAL_SERVER_ERROR)) 
            {
//            	log.info("searchBasedOnFace 7:  Usuario no encontrado");
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            log.error("Error searching facial by id with message: {}", e.getMessage());
        }
        return null;
    }
    
    
    /**
     * Performs a search based on fingers
     *
     * @param leftIndex  - Base64 String
     * @param rightIndex - Base64 String
     * @param id         - the id associated
     * @return - 200 if found, 404 otherwise
     */
    public ResponseEntity<String> searchBasedOnFingers(String leftLitle, String leftRing, String leftMiddle, String leftIndex, String leftThumb, String rightLittle, String rightRing, String rightMiddle, String rightIndex, String rightThumb, String id, String imageType) {
    	log.info("INFO: MBSSHandler.searchBasedOnFingers: "
//    			+ "leftLitle:::"+leftLitle+"\n"
//    			+ "leftRing::::"+leftRing+"\n"
//    			+ "leftMiddle::" +leftMiddle+"\n"
//    			+ "leftIndex:::"+leftIndex+"\n"
//    			+ "leftThumb:::" +leftThumb+"\n"
//    			+ "rightLittle:"+rightLittle+"\n"
//    			+ "rightRing:::"+rightRing+"\n"
//    			+ "rightMiddle:"+rightMiddle+"\n"
//    			+ "rightIndex::"+rightIndex+"\n"
//    			+ "rightThumb::"+rightThumb+"\n"
    			+ "id::::::::::"+id+"\n"
    			+ "imageType:::"+imageType);
    	try {	
	        JSONObject requestJson = new JSONObject();
	        requestJson.put("id", id);
	        requestJson.put("fingers", getFingers(leftLitle, leftRing, leftMiddle, leftIndex, leftThumb, rightLittle, rightRing, rightMiddle, rightIndex, rightThumb));
	        requestJson.put("imageType", imageType);
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
	        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/fingers";
	        RestTemplate restTemplate = new RestTemplate();
	        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
	        log.info("INFO: MBSSHandler.searchBasedOnFingers: (1) "+responseEntity.toString());
	        return responseEntity;
    	} catch (Exception e) {
    		 log.info("ERROR: MBSSHandler.searchBasedOnFingers:",e);
		}
		return null;
    }
    
    /**
     * Codigo optimizado para Performs a search based on fingers
     * @param requestJson
     * @return
     */
    public ResponseEntity<String> searchBasedOnFingers(JSONObject requestJson){
    	try {		       
	        HttpHeaders headers = new HttpHeaders();
	        headers.setContentType(MediaType.APPLICATION_JSON);
	        HttpEntity<String> entity = new HttpEntity<>(requestJson.toString(), headers);
	        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/fingers";
	        RestTemplate restTemplate = new RestTemplate();
	        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
	        log.info("INFO: MBSSHandler.searchBasedOnFingers: (1) "+responseEntity.toString());
	        return responseEntity;
    	} catch (Exception e) {
    		 log.info("ERROR: MBSSHandler.searchBasedOnFingers: ",e);
		}
		return null;
    }

    public ResponseEntity<String> searchBasedOnSlaps(String leftSlap, String rightSlap, String thumbsSlap, String id) {
    	log.info("INFO: MBSSHandler.searchBasedOnSlaps: ");
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/slaps";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        JSONObject internal = getSlaps(leftSlap, rightSlap, thumbsSlap);
        jsonObject.put("slaps", internal);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(jsonObject.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
        log.info("INFO: MBSSHandler.searchBasedOnSlaps: "+responseEntity);
        return responseEntity;
    }

    public ResponseEntity<String> searchBasedOnSlapsAndId(String leftSlap, String rightSlap, String thumbsSlap, String id) {
    	log.info("INFO: MBSSHandler.searchBasedOnSlapsAndId ");
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/search/slapsId";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", id);
        JSONObject internal = getSlaps(leftSlap, rightSlap, thumbsSlap);
        jsonObject.put("slaps", internal);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
        log.info("INFO: MBSSHandler.searchBasedOnSlapsAndId "+responseEntity);
        return responseEntity;
    }

    public ResponseEntity<String> matchFacial(String b64face1, String b64face2, int umbral){
    	log.info("INFO: MBSSHandler.matchFacial 1");
        String uri = findUrl(mbssBaseUrl, "mbss", discoveryClient) + "/compareFacial";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("facialB641", b64face1);
        jsonObject.put("facialB642", b64face2);
        jsonObject.put("score", umbral);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(jsonObject.toString(), headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(uri, entity, String.class);
        log.info("INFO: MBSSHandler.matchFacial  "+responseEntity);
        return responseEntity;
    }

    /**
     * Helper method
     *
     * @param leftIndex  - Base64 String
     * @param rightIndex - Base64 String
     * @return
     */
    private JSONObject getFingers(String leftLitle, String leftRing, String leftMiddle, String leftIndex, String leftThumb, String rightLittle, String rightRing, String rightMiddle, String rightIndex, String rightThumb) {
    	log.info("INFO: MBSSHandler.getFingers 1");
        JSONObject fingersJson = new JSONObject();
        String nStr = null;
        fingersJson.put("li", leftIndex);
        fingersJson.put("lt", leftThumb);
        fingersJson.put("lm", leftMiddle);
        fingersJson.put("lr", leftRing);
        fingersJson.put("ll", leftLitle);
        fingersJson.put("ri", rightIndex);
        fingersJson.put("rt", rightThumb);
        fingersJson.put("rm", rightMiddle);
        fingersJson.put("rr", rightRing);
        fingersJson.put("rl", rightLittle);
        log.info("INFO: MBSSHandler.getFingers ->>");
        return fingersJson;
    }

    private JSONObject getSlaps(String leftSlap, String rightSlap, String slapThumbs) 
    {
    	log.info("INFO: MBSSHandler.getSlaps 1");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("ls", leftSlap);
        jsonObject.put("rs", rightSlap);
        jsonObject.put("ts", slapThumbs);
        return jsonObject;
    }
}
