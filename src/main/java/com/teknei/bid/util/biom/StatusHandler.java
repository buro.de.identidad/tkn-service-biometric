package com.teknei.bid.util.biom;

import com.teknei.bid.dto.BiometricDetailDTO;
import com.teknei.bid.dto.BiometricScanRelationDTO;
import com.teknei.bid.dto.BiometricSlapsDTO;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Amaro on 02/08/2017.
 */
public class StatusHandler {

    private StatusHandler() {
        mapInstance = new HashMap<>();
        slapsInstance = new HashMap<>();
        slapsDetailInstance = new HashMap<>();
        scanBiomRelationMap = new HashMap<>();
        slapsMap = new HashMap<>();
        registrationStatusMap = new HashMap<>();
    }

    private static StatusHandler instance = new StatusHandler();
    private Map<String, Integer> mapInstance;
    private Map<String, Integer> slapsInstance;
    private Map<String, BiometricDetailDTO> slapsDetailInstance;
    private Map<String, BiometricScanRelationDTO> scanBiomRelationMap;
    private Map<String, BiometricSlapsDTO> slapsMap;
    private Map<String, String> registrationStatusMap;

    public static StatusHandler getInstance() {
        return instance;
    }

    public void initCaptureFor(String biometricId) {
        mapInstance.put(biometricId, 1);
    }

    public void endCaptureFor(String biometricId) {
        mapInstance.put(biometricId, 0);
    }

    public void errorCaptureFor(String biometricId){
        mapInstance.put(biometricId, -1);
    }

    public Integer getStatusFor(String biometricId) {
        return mapInstance.get(biometricId);
    }

    public Integer getSlapStatusFor(String biometricId) {
        return slapsInstance.get(biometricId);
    }

    public void slapCaptureRight(String biometricId) {
        slapsInstance.put(biometricId, 1);
    }

    public void slapCaptureLeft(String biometricId) {
        slapsInstance.put(biometricId, 2);
    }

    public void slapCaptureThumbs(String biometricId) {
        slapsInstance.put(biometricId, 3);
    }

    public void slapCaptureDone(String biometricId) {
        slapsInstance.put(biometricId, 0);
    }

    public void slapCaptureZero(String biometricId) {
        slapsInstance.put(biometricId, -1);
    }

    public void initDetailFor(String biometricId, BiometricDetailDTO detailDTO) {
        slapsDetailInstance.put(biometricId, detailDTO);
    }

    public void addRelation(String biometricId, String scanId, Long idCustomer) {
        BiometricScanRelationDTO dto = new BiometricScanRelationDTO();
        dto.setBiometricId(biometricId);
        dto.setIdCustomer(idCustomer);
        dto.setScanId(scanId);
        scanBiomRelationMap.put(biometricId, dto);
    }

    public BiometricScanRelationDTO getScanForBiom(String biometricId) {
        return scanBiomRelationMap.get(biometricId);
    }

    public BiometricDetailDTO findDetailForBiometricId(String biometricId) {
        return slapsDetailInstance.get(biometricId);
    }

    public void addSlapsData(String biometricId, BiometricSlapsDTO dto) {
        slapsMap.put(biometricId, dto);
    }

    public BiometricSlapsDTO getSlapsData(String biometricId) {
        return slapsMap.get(biometricId);
    }

    public void registerEndStatus(String biometricId, String status){
        registrationStatusMap.put(biometricId, status);
    }

    public String getEndStatusForBiometricId(String biometricId){
        return registrationStatusMap.get(biometricId);
    }

}