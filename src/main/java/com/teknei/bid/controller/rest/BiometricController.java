package com.teknei.bid.controller.rest;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.google.gson.Gson;
import com.teknei.bid.command.Command;
import com.teknei.bid.command.CommandRequest;
import com.teknei.bid.command.CommandResponse;
import com.teknei.bid.command.RequestType;
import com.teknei.bid.command.Status;
import com.teknei.bid.common.utils.JsonMaker;
import com.teknei.bid.common.utils.LogUtil;
import com.teknei.bid.dto.BiometricCaptureRequestIdentDTO;
import com.teknei.bid.dto.ResponseBase;
import com.teknei.bid.persistence.entities.BidClieCurp;
import com.teknei.bid.persistence.entities.BidClieReasonFinger;
import com.teknei.bid.persistence.entities.BidUsua;
import com.teknei.bid.persistence.entities.BidValUsr;
import com.teknei.bid.persistence.repository.BidClieReasonFingerRepository;
import com.teknei.bid.persistence.repository.BidCurpRepository;
import com.teknei.bid.persistence.repository.BidUsuaRepository;
import com.teknei.bid.persistence.repository.BidValUsrRepository;
import com.teknei.bid.util.biom.BiometricStatusSingleton;
import com.teknei.bid.util.biom.MBSSHandler;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
@RestController
@RequestMapping(value = "/biometric")
public class BiometricController {

    @Autowired
    @Qualifier(value = "biometricCommand")
    private Command biometricCommand;
    @Autowired
    private MBSSHandler mbssHandler;
   
    @Autowired
    private BidClieReasonFingerRepository bidClieReasonFingerRepository; 
    
    @Autowired
    private BidUsuaRepository bidUsuaRepository;    
    
    @Autowired
    private BidValUsrRepository bidValUsrRepository;
    
    @Autowired
    private BidCurpRepository bidCurpRepository;
    
    private static final Logger log = LoggerFactory.getLogger(BiometricController.class);

    @ApiOperation(value = "Uploads the biometric data related to the customer given by the url", notes = "The last parameter in the url specifies the type, 1 for fingers, 2 for slaps. The request json string must be formed as following: {ls,rs,ts} or {ll,lr,lm,li,lt,rl,rr,rm,ri,rt}. The return message includes 409 if the data is already registered for other customer, 500 if its internal server error or 200 if its ok", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The biometric data is processed correctly"),
            @ApiResponse(code = 422, message = "The biometric data could not be processes by the biometric motor")
    })
    @RequestMapping(value = "/upload/{id}/{type}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addMinucias(@RequestBody String jsonRequest, @PathVariable Long id, @PathVariable Integer type) 
    {
    	JSONObject jsonObject = new JSONObject(jsonRequest);
    	log.info("INFO: BiometricController.addMinucias: type:"+type+ " id: "+id);
        Integer ineType = 0;
        if(type.equals(11)||type.equals(10)){
            ineType = type;
            type = 1;
        }
       
    	log.info("INFO: >>>>>>>>>"+LogUtil.logJsonObject(new JSONObject(jsonRequest)));
    	//TODO validar huellas
    	/*if (!validate6fingers(jsonRequest)) {//TODO minimo 6 huellas.    
    		if (jsonObject.optString("reasonFingers").isEmpty()) {// TODO validar objeto reasonfingers	
	    		CommandResponse commandResponse = new CommandResponse();
	    		commandResponse.setDesc("Bad Request number of fingers min than 6");
				return new ResponseEntity<>(commandResponse.getDesc(), HttpStatus.UNPROCESSABLE_ENTITY);
    		}
		}*/
    	//TODO motivos de omision
    	log.info("INFO: reasonFingers:");
    	log.info("INFO: > "+jsonObject.optString("reasonFingers"));
		if (!jsonObject.optString("reasonFingers").isEmpty()) {// TODO validar objeto reasonfingers
			List<BidClieReasonFinger> reasonList = bidClieReasonFingerRepository.findAllByIdClie(id);	
			// todo meter en ciclo para numero de dedos omitidos.
			JSONArray reasonFingers = jsonObject.optJSONArray("reasonFingers");
//			String tempCode = "";
			for (int i = 0; i < reasonFingers.length(); i++) {
				JSONObject reason = reasonFingers.getJSONObject(i);		
//				if ((!tempCode.equals(reason.optString("code")))) {
					if(!existReason(reasonList, reason.optString("finger"))) {
						//busca registro previo
						bidClieReasonFingerRepository.findAllByIdClie(id);
						BidClieReasonFinger bidClieReasonFinger = new BidClieReasonFinger();
						bidClieReasonFinger.setIdClie(id);
						bidClieReasonFinger.setFinger(reason.optString("finger"));
						bidClieReasonFinger.setCode(reason.optString("code"));
						bidClieReasonFinger.setDescription(reason.optString("description"));
						bidClieReasonFinger = bidClieReasonFingerRepository.save(bidClieReasonFinger);
						log.info("INFO: SaveReasonFinger: " + bidClieReasonFinger.getCode());
//						tempCode = bidClieReasonFinger.getCode();
					}
//				}
			}
		}
		
    	if(type==9)
    		return addMinuciasForce(jsonRequest, id,1);
    	
        CommandRequest request = new CommandRequest();
        switch (type) {
            case 1:
                request.setRequestType(RequestType.BIOM_FINGERS_REQUEST);
                break;
            default:
                request.setRequestType(RequestType.BIOM_SLAPS_REQUEST);
                break;
        }
        request.setId(id);
        log.info("INFO: BiometricController.addMinucias: "+ LogUtil.logJsonObject(jsonObject));
        String username = jsonObject.getString("username");
        request.setUsername(username);
        jsonRequest = jsonRequest.replace("\\\\", "\\");
        request.setData(jsonRequest);
        request.setForzeAddFinger(false);

        //INETYPE
        request.setIneType(ineType);
        

        CommandResponse commandResponse = biometricCommand.execute(request);
        if (!commandResponse.getStatus().equals(Status.BIOM_OK)) {
            log.debug(" BiometricController - Returning: {} {}", commandResponse, HttpStatus.UNPROCESSABLE_ENTITY);
            return new ResponseEntity<>(commandResponse.getDesc(), HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            return new ResponseEntity<>(commandResponse.getDesc(), HttpStatus.OK);
        }
    }
    
    private boolean existReason(List<BidClieReasonFinger> reasonList, String finger) {
	
		for(BidClieReasonFinger rf : reasonList ) {
			if(rf.getFinger().equalsIgnoreCase(finger)) {
		    	return true; 
			}
		}    	
    	return false;     	
    }
    
	private static boolean validate6fingers(String jsonRequest) {	
		JSONObject jsonObject = new JSONObject(jsonRequest);
		Integer numOfFingers = 0;
		numOfFingers = !jsonObject.optString("lt").isEmpty() ? numOfFingers + 1 : numOfFingers;
		numOfFingers = !jsonObject.optString("li").isEmpty() ? numOfFingers + 1 : numOfFingers;
		numOfFingers = !jsonObject.optString("lm").isEmpty() ? numOfFingers + 1 : numOfFingers;
		numOfFingers = !jsonObject.optString("lr").isEmpty() ? numOfFingers + 1 : numOfFingers;
		numOfFingers = !jsonObject.optString("ll").isEmpty() ? numOfFingers + 1 : numOfFingers;
		numOfFingers = !jsonObject.optString("rt").isEmpty() ? numOfFingers + 1 : numOfFingers;
		numOfFingers = !jsonObject.optString("ri").isEmpty() ? numOfFingers + 1 : numOfFingers;
		numOfFingers = !jsonObject.optString("rm").isEmpty() ? numOfFingers + 1 : numOfFingers;
		numOfFingers = !jsonObject.optString("rr").isEmpty() ? numOfFingers + 1 : numOfFingers;
		numOfFingers = !jsonObject.optString("rl").isEmpty() ? numOfFingers + 1 : numOfFingers;
		log.info("INFO: numOfFingers"+numOfFingers);
		return numOfFingers >= 6;
	}

    @ApiOperation(value = "Uploads the biometric data related to the customer given by the url", notes = "The last parameter in the url specifies the type, 1 for fingers, 2 for slaps. The request json string must be formed as following: {ls,rs,ts} or {ll,lr,lm,li,lt,rl,rr,rm,ri,rt}. The return message includes 409 if the data is already registered for other customer, 500 if its internal server error or 200 if its ok", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The biometric data is processed correctly"),
            @ApiResponse(code = 422, message = "The biometric data could not be processes by the biometric motor")
    })
    @RequestMapping(value = "/uploadForce/{id}/{type}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> addMinuciasForce(@RequestBody String jsonRequest, @PathVariable Long id, @PathVariable Integer type)
    {
    	log.info("INFO: BiometricController.addMinuciasForce: type:"+type+ " id:"+id);
        CommandRequest request = new CommandRequest();
        switch (type) {
            case 1:
                request.setRequestType(RequestType.BIOM_FINGERS_REQUEST);
                break;
            default:
                request.setRequestType(RequestType.BIOM_SLAPS_REQUEST);
                break;
        }
        request.setId(id);
        JSONObject jsonObject = new JSONObject(jsonRequest);
        log.info("INFO: BiometricController.addMinuciasForce: "+ LogUtil.logJsonObject(jsonObject));
        String username = jsonObject.getString("username");
        request.setUsername(username);
        jsonRequest = jsonRequest.replace("\\\\", "\\");
        request.setData(jsonRequest);
        request.setForzeAddFinger(true);
        CommandResponse commandResponse = biometricCommand.execute(request);
        if (!commandResponse.getStatus().equals(Status.BIOM_OK)) {
            log.info("INFO: BiometricController.addMinuciasForce: Returning: lbl:uploadforce {} {}", commandResponse, HttpStatus.UNPROCESSABLE_ENTITY);
            return new ResponseEntity<>(commandResponse.getDesc(), HttpStatus.UNPROCESSABLE_ENTITY);
        } else {
            return new ResponseEntity<>(commandResponse.getDesc(), HttpStatus.OK);
        }
    }

    private ResponseEntity<String> parseHttpError(HttpClientErrorException he) 
    {
    	log.error("ERROR: BiometricController.parseHttpError: "+he.getMessage());
        if (he.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
            return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
        } else if (he.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
            return new ResponseEntity<>("Peticion mal formada", HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customer", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFinger(@RequestBody String jsonStringRequest) 
    {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
    	log.info("INFO: BiometricController.searchByFinger: "+LogUtil.logJsonObject(jsonObject));
        String idReceived = jsonObject.optString("id", "99");
//        String llReceived = jsonObject.optString("ll", null);
//        String liReceived = jsonObject.optString("li", null);
//        String lmReceived = jsonObject.optString("lm", null);
//        String lrReceived = jsonObject.optString("lr", null);
//        String ltReceived = jsonObject.optString("lt", null);
//        String rlReceived = jsonObject.optString("rl", null);
//        String riReceived = jsonObject.optString("ri", null);
//        String rmReceived = jsonObject.optString("rm", null);
//        String rrReceived = jsonObject.optString("rr", null);
//        String rtReceived = jsonObject.optString("rt", null);
        
        String imageTypeReceived = jsonObject.optString("contentType", "wsq");
        if (imageTypeReceived.contains("/")) {
            String[] contents = imageTypeReceived.split("\\/");
            imageTypeReceived = contents[contents.length - 1];
        }
        try {
        	
        	log.info("INFO: BiometricController.searchByFinger: searchBasedOnFingers...");
//          ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnFingers(llReceived, lrReceived, lmReceived, liReceived, ltReceived, rlReceived, rrReceived, rmReceived, riReceived, rtReceived, idReceived, imageTypeReceived);
        	
        	//--Optimizacion de codigo->>>
        	ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnFingers(
        			JsonMaker.requestFingersJson(JsonMaker.CreatefingersJson(jsonObject), idReceived, imageTypeReceived)); 
        	//--Optimizacion de codigo->>>         	
        	log.info("INFO: BiometricController.searchByFinger: searchBasedOnFingers SUCSESS!!");
            return responseEntity;
        } catch (HttpClientErrorException he) {    		
        	log.error("ERROR: BiometricController.searchByFinger: HttpClientErrorException :",he);
            if (he.getStatusCode().equals(HttpStatus.NOT_FOUND)) {  
            	log.error("ERROR: BiometricController.searchByFinger: Usuario no encontrado");
                return new ResponseEntity<>("Usuario no encontrado", HttpStatus.NOT_FOUND);
            } 
            if (he.getStatusCode().equals(HttpStatus.BAD_REQUEST)) {
            	log.error("ERROR: BiometricController.searchByFinger: Peticion mal formada");
               return new ResponseEntity<>("Peticion mal formada", HttpStatus.BAD_REQUEST);
            }
            return new ResponseEntity<>("Peticion mal formada", HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
        	log.info("ERROR: BiometricController.searchByFinger: UnespectedException: Error buscando al usuario :",e);
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }	
    }
    /**
     * Genera fecha de bloqueo 
     * @return now + 10 min
     */
    private Timestamp lokedDate() {
    	Calendar cal = Calendar.getInstance();
    	cal.setTime( new Timestamp(System.currentTimeMillis()));
    	cal.add(Calendar.MINUTE,9);
    	return new Timestamp(cal.getTimeInMillis());
    }
    /**
     * Verifica existencia de usuario en bid
     * @param us
     * @return
     */
    private BidUsua verifyIdClie(Long us) {
    	List<BidUsua> bidUserList = bidUsuaRepository.findByIdClie(us);
    	BidUsua bidUser = null;
    	if(bidUserList == null||bidUserList.isEmpty()) {
    		log.info("ERROR: BiometricController.verifyIdClie: no se encontro al usuario: us:"+us);
    		return null;
    	}else {
    		bidUser = new BidUsua();
    		bidUser = bidUserList.get(0);
    	}
    	return bidUser;
    } 

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {ll,li,lm,lr,lt,rl,ri,rm,rr,rt}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFingerAndId(@RequestBody String jsonStringRequest){
     // Metodo de validacion biometrica dede usuario 	
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
        log.info("INFO: BiometricController.searchByFingerAndId: "+ LogUtil.logJsonObject(jsonObject));
        String idReceived = jsonObject.optString("id", "1");
        String imageTypeReceived = jsonObject.optString("contentType", "wsq");
        if (imageTypeReceived.contains("/")) {
            String[] contents = imageTypeReceived.split("\\/");
            imageTypeReceived = contents[contents.length - 1];
        }
        try {        	
        	JSONObject fingersJson = JsonMaker.CreatefingersJson(jsonObject);
        	ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnFingersById(
        			 JsonMaker.requestFingersJson(fingersJson, idReceived, imageTypeReceived));

    		//TODO Proceso para validar otro id relacionado al mismo curp.
        	if(responseEntity.getStatusCode().equals((HttpStatus.NOT_FOUND))) {
        		List<BidClieCurp> clieCurpList = getSecondType(Long.parseLong(idReceived));
        		for(BidClieCurp clieCurp: clieCurpList) {
            		responseEntity = mbssHandler.searchBasedOnFingersById(
               			 JsonMaker.requestFingersJson(fingersJson,""+clieCurp.getIdClie(), imageTypeReceived));
                    if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
                    	break;
                    }
            	}        		
        	}   
        	
        	log.info("INFO: BiometricController.searchByFingerAndId: BODY: "+responseEntity.getBody());
            if(!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
            	BidValUsr vuser =  bidValUsrRepository.findByUser(verifyIdClie(Long.valueOf(idReceived)).getIdUsua());             	
            	vuser.setIntentos(vuser.getIntentos()+1);
            	log.info("INFO: BiometricController.searchByFingerAndId: Intentos para bloqueo: " + ((vuser.getIntentos() - 5) * (-1)));
            	if (vuser.getIntentos() > 4) {					
					vuser.setFbloqueo(lokedDate());
					log.info("INFO: Cuenta bloqueada - Fbloqueo: " + lokedDate());
					vuser.setIntentos(0);
					bidValUsrRepository.save(vuser);
					return new ResponseEntity<>(new Gson().toJson(new ResponseBase(HttpStatus.LOCKED.toString(),
							"Cuenta bloqueada intente dentro de 10 minutos", "FAIL")).toString(), HttpStatus.OK);
				}
				if (vuser.getIntentos() > 2) {					
					bidValUsrRepository.save(vuser);
					return new ResponseEntity<>(new Gson().toJson(new ResponseBase(HttpStatus.LOOP_DETECTED.toString(),
											"Te quedan " + ((vuser.getIntentos() - 5) * (-1))+ " intentos para bloqueo temporal",
											"FAIL")).toString(),HttpStatus.OK);

				}				
        		bidValUsrRepository.save(vuser);
            	return new ResponseEntity<>(new Gson().toJson(new ResponseBase(HttpStatus.BAD_REQUEST.toString(),
    					"Las huellas ingresadas no corresponden a ninguna de las registradas, favor de verificar.",
    					"FAIL")).toString(), HttpStatus.OK); 
            }else {
	            return new ResponseEntity<>(new Gson().toJson(new ResponseBase(	
	            		responseEntity.getStatusCode().toString(),
	            		responseEntity.getBody(),
	            		responseEntity.getStatusCode().toString().equals("200")?"OK":"FAIL")
	            		).toString()
	            		, HttpStatus.OK);
            }
        } catch (HttpClientErrorException he) {
        	log.error("ERROR: BiometricController.searchByFingerAndId: HttpClientErrorException: ",he); 
        	return new ResponseEntity<>(new Gson().toJson(new ResponseBase(HttpStatus.BAD_REQUEST.toString(),
					"Error al realizar la peticion",
					"FAIL")).toString(), HttpStatus.OK); 
        } catch (Exception e) {
        	log.error("ERROR: BiometricController.searchByFingerAndId: Unespected Exception: ",e); 
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * Metodo para buscar la huella ingresada con otra registrada previamente.
     * @param idClie
     * @return
     */
    private List<BidClieCurp> getSecondType(Long idClie){    	
		// buscando curp del id ingresado.
    	BidClieCurp clieCurp =  bidCurpRepository.findTopByIdClie(idClie);    	
		// buscando registros relacionados con ese curp.
    	List<BidClieCurp> curps = bidCurpRepository.findAllByCurp(clieCurp.getCurp());
    	// removiendo el id original de la lista.
    	if(curps.size()>1) {
    		curps.remove(clieCurp);
    	}else {
    		curps = null;
    	}
    	// Retornando lista de ids relacionados.
    	return curps;
    }
    

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerSlaps", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchBySlaps(@RequestBody String jsonStringRequest) 
    {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
    	log.info("INFO: BiometricController.searchBySlaps: "+LogUtil.logJsonObject(jsonObject));
        String slReceived = jsonObject.optString("sl");
        if (slReceived == null || slReceived.isEmpty()) {
            slReceived = jsonObject.optString("ls");
        }
        String srReceived = jsonObject.optString("sr");
        if (srReceived == null || srReceived.isEmpty()) {
            srReceived = jsonObject.optString("rs");
        }
        String stReceived = jsonObject.optString("st");
        if (stReceived == null || stReceived.isEmpty()) {
            stReceived = jsonObject.optString("ts");
        }
        String idReceived = jsonObject.optString("id");
        if (idReceived == null || idReceived.isEmpty()) {
            idReceived = jsonObject.optString("operationId");
            if (idReceived == null || idReceived.isEmpty()) {
                Integer idOpt = jsonObject.optInt("operationId");
                if (idOpt != null) {
                    idReceived = String.valueOf(idOpt);
                }
            }
        }
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnSlaps(slReceived, srReceived, stReceived, idReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,sl,sr,st}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerSlapsId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchBySlapsId(@RequestBody String jsonStringRequest) 
    {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
    	log.info("INFO: BiometricController.searchBySlapsId: "+LogUtil.logJsonObject(jsonObject));
        String slReceived = jsonObject.optString("sl");
        String srReceived = jsonObject.optString("sr");
        String stReceived = jsonObject.optString("st");
        String idReceived = jsonObject.optString("id");
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnSlapsAndId(slReceived, srReceived, stReceived, idReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerFace", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFace(@RequestBody String jsonStringRequest) 
    {
    	log.info("INFO: BiometricController.searchByFace ");
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
        String faceReceived = jsonObject.optString("facial");
        String idReceived = jsonObject.optString("id");
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnFace(idReceived, faceReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Finds the customer related to the given biometric data", notes = "The request must be a valid json like the following: {id,facial}", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "The customer is found"),
            @ApiResponse(code = 404, message = "The customer is not found"),
            @ApiResponse(code = 500, message = "Internal server error")
    })
    @RequestMapping(value = "/search/customerFaceId", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public ResponseEntity<String> searchByFaceId(@RequestBody String jsonStringRequest) 
    {

//    	log.info("lbl: searchByFaceId "+jsonStringRequest);
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
    	log.info("INFO: BiometricController.searchByFaceId "+ LogUtil.logJsonObject(jsonObject));
        String faceReceived = jsonObject.optString("facial");
        String idReceived = jsonObject.optString("id");
        try {
            ResponseEntity<String> responseEntity = mbssHandler.searchBasedOnFaceId(idReceived, faceReceived);
            return responseEntity;
        } catch (HttpClientErrorException he) {
            return parseHttpError(he);
        } catch (Exception e) {
            return new ResponseEntity<>("Error buscando al usuario", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "Starts a process on biometric hardware", response = String.class)
    @RequestMapping(value = "/start", method = RequestMethod.POST)
    public String initCaptureFor(@RequestBody String serialNumber) 
    {
    	log.info("INFO: BiometricController.initCaptureFor: serialNumber: "+serialNumber);
        try {
            JSONObject jsonObject = new JSONObject(serialNumber);
            String serial = jsonObject.getString("biometricId");
            String operationId = jsonObject.getString("operationId");
            BiometricStatusSingleton.getInstance().initCapture(serial);
            BiometricStatusSingleton.getInstance().addRelationSerialId(serial, operationId);
            return "0";
        } catch (Exception e) {
            log.error("ERROR: BiometricController.initCaptureFor Error for initCapture: {} with message: {}", serialNumber, e.getMessage());
        }
        return "1";
    }

    @ApiOperation(value = "Gets the status for the requested serial number", response = String.class)
    @RequestMapping(value = "/status/{serialNumber}", method = RequestMethod.GET)
    public String getStatusFor(@PathVariable String serialNumber) 
    {
    	log.info("INFO: BiometricController.getStatusFor: serialNumber: "+serialNumber);
        Integer foundActive = BiometricStatusSingleton.getInstance().getStatusFor(serialNumber);
        if (foundActive == 1) {
            BiometricStatusSingleton.getInstance().endCapture(serialNumber);
            String id = BiometricStatusSingleton.getInstance().getIdFromSerial(serialNumber);
            return "1|" + id;
        }
        return "0";
    }
    @Transactional
    @ApiOperation(value = "Gets the status for the requested serial number", response = String.class)
    @RequestMapping(value = "/isCorrectforce/{idOld}/{idNew}", method = RequestMethod.GET)
    public String getStatusFor(@PathVariable String idOld,@PathVariable String idNew) 
    {
    	log.info("INFO: BiometricController.getStatusFor: idOld:"+idOld+" idNew:"+idNew);
    	BidClieCurp   clieCurpRecepcion = (BidClieCurp) bidCurpRepository.findTopByIdClie(new Long(idOld));
        log.info("INFO: BiometricController.getStatusFor: clieCurpRecepcion :: " +clieCurpRecepcion.getCurp() + "  ::: " + clieCurpRecepcion.getIdClie() + "  ::: " + clieCurpRecepcion.getIdTipo());
        BidClieCurp clieCurpRespuesta = (BidClieCurp) bidCurpRepository.findTopByIdClie(new Long(idNew));
        log.info("INFO: BiometricController.getStatusFor: clieCurpRespuesta  " +clieCurpRespuesta.getCurp() + "  ::: " + clieCurpRespuesta.getIdClie() + "  ::: " + clieCurpRespuesta.getIdTipo());
        if(clieCurpRecepcion.getCurp().toUpperCase().equals(clieCurpRespuesta.getCurp().toUpperCase()))
        {
        	log.info("INFO: BiometricController.getStatusFor:  ****Hay 2 curp iguales****");
        	if(clieCurpRecepcion.getIdTipo() == clieCurpRespuesta.getIdTipo())
        	{
        		log.info("INFO: BiometricController.getStatusFor:  ****Tipos iguales****");
        		if(idOld.equals(idNew))
        		{
        			log.info("INFO: BiometricController.getStatusFor:  ****Id iguales****");
        			return "1";
        		}
        		else
        			return "0";
        		
        	}
        	else 
        	{
        		log.info("INFO: BiometricController.getStatusFor: Usuario de CURP  Existentes , pero con Tipo diferente"+
        				"\nclieCurpRecepcion::"+ clieCurpRecepcion.getIdClie() +
        				"\nclieCurpRespuesta::"+ clieCurpRespuesta.getIdClie() );
        		 updateCliente(clieCurpRecepcion.getIdClie(), clieCurpRespuesta.getIdClie());
        		 updateCliente(clieCurpRespuesta.getIdClie(), clieCurpRecepcion.getIdClie());
        		 return "1"; 
        	}
        }
        return "0";
    }
    
    @Transactional
    public void updateCliente(Long idClient, Long idClientRel)
    {   
    	log.info("INFO: BiometricController.updateCliente: :: [tkn-service-client] :: "+this.getClass().getName()+".updateCliente ["+idClient+","+idClientRel+"]");
    	try 	
		{    
			bidCurpRepository.updateIdclierel( idClient , idClientRel );
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }
    
    
    @ApiOperation(value = "Obtiene Relacion de cliente - cliente por perfil", response = String.class)
    @RequestMapping(value = "/getidClieRelacion/{id}", method = RequestMethod.GET)
    public String getStatusRelacion(@PathVariable String id) 
    {	
    	log.info("INFO: BiometricController.getStatusRelacion :: [tkn-service-client] ::  "+id);
    	BidClieCurp   clieCurpRecepcion = (BidClieCurp) bidCurpRepository.findTopByIdClie(new Long(id));
    	log.info("INFO: BiometricController.getStatusRelacion :: [tkn-service-client] ::  {}",clieCurpRecepcion);
    	return ""+clieCurpRecepcion.getRel();
    }
    
    

    @ApiOperation(value = "Starts process on client for verification on biometric system", response = String.class, notes = "Response 1 is error, response 0 is OK")
    @RequestMapping(value = "/startAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String initCaptureForIdentification(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) 
    {
    	log.info("INFO: BiometricController.initCaptureForIdentification:");
        try {
            BiometricStatusSingleton.getInstance().initCaptureAuth(requestIdentDTO);
            return "0";
        } catch (Exception e) {
            log.error("ERROR: BiometricController.initCaptureForIdentification: Error for initAuthCapture for: {} with message: {}", requestIdentDTO, e.getMessage());
        }
        return "1";
    }

    @ApiOperation(value = "Starts process on client for verification on biometric system", response = String.class, notes = "Response 1 is error, response 0 is OK")
    @RequestMapping(value = "/startSign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String initCaptureSignForIdentification(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) 
    {
    	log.info("INFO: BiometricController.initCaptureSignForIdentification: "+requestIdentDTO);
        try {
            BiometricStatusSingleton.getInstance().initCaptureSign(requestIdentDTO);
            return "0";
        } catch (Exception e) {
            log.error("ERROR: BiometricController.initCaptureSignForIdentification: Error for initAuthCapture for: {} with message: {}", requestIdentDTO, e.getMessage());
        }
        return "1";
    }

    @ApiOperation(value = "Finds the status of the process for given serial number", response = BiometricCaptureRequestIdentDTO.class)
    @RequestMapping(value = "/statusAuth/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BiometricCaptureRequestIdentDTO getStatusForAuth(@PathVariable String serial) 
    {
    	log.info("INFO: BiometricController.getStatusForAuth: serial:"+serial);
        BiometricCaptureRequestIdentDTO requestIdentDTO = BiometricStatusSingleton.getInstance().findCaptureDataFor(serial);
        if (requestIdentDTO == null) {
            return null;
        }
        Integer status = BiometricStatusSingleton.getInstance().getStatusForAuth(requestIdentDTO);
        if (status == 1) {
            log.info("Status 1");
            BiometricStatusSingleton.getInstance().endCaptureAuth(requestIdentDTO);
            return requestIdentDTO;
        }
        return null;
    }

    @ApiOperation(value = "Finds the status of the process for given serial number", response = BiometricCaptureRequestIdentDTO.class)
    @RequestMapping(value = "/statusSign/{serial}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BiometricCaptureRequestIdentDTO getStatusForAuthSign(@PathVariable String serial) 
    {
    	log.info("INFO: BiometricController.getStatusForAuthSign: serial: "+serial);
        BiometricCaptureRequestIdentDTO requestIdentDTO = BiometricStatusSingleton.getInstance().findCaptureSignDataFor(serial);
        if (requestIdentDTO == null) {
            return null;
        }
        Integer status = BiometricStatusSingleton.getInstance().getStatusForSign(requestIdentDTO);
        if (status == 1) {
            log.info("Status 1 for sign");
            BiometricStatusSingleton.getInstance().endCaptureSign(requestIdentDTO);
            return requestIdentDTO;
        }
        return null;
    }


    @ApiOperation(value = "Confirms the current capture result for the associated serial number and customer id")
    @RequestMapping(value = "/confirmAuth/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String confirmCaptureAuth(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO, @PathVariable Integer status) {
        BiometricStatusSingleton.getInstance().confirmStatus(requestIdentDTO, status);
        return "0";
    }

    @ApiOperation(value = "Confirms the current capture result for the associated serial number and customer id")
    @RequestMapping(value = "/confirmSign/{status}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String confirmCaptureSign(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO, @PathVariable Integer status) {
        BiometricStatusSingleton.getInstance().confirmStatusSign(requestIdentDTO, status);
        return "0";
    }

    @ApiOperation(value = "Queries the results for the given data")
    @RequestMapping(value = "/queryAuth", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String queryCaptureAuth(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) {
        Integer status = BiometricStatusSingleton.getInstance().queryStatus(requestIdentDTO);
        return String.valueOf(status);
    }

    @ApiOperation(value = "Queries the results for the given data")
    @RequestMapping(value = "/queryAuthSign", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String queryCaptureSign(@RequestBody BiometricCaptureRequestIdentDTO requestIdentDTO) {
        Integer status = BiometricStatusSingleton.getInstance().queryStatusSign(requestIdentDTO);
        return String.valueOf(status);
    }

    @RequestMapping(value = "/searchDetail/{id}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public ResponseEntity<String> findDetailRecord(@PathVariable String id) {
        return mbssHandler.searchDetailById(id);
    }

    @RequestMapping(value = "/getHash/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<String> getHashForCustomer(@PathVariable String id) {
        return mbssHandler.getHashForCustomer(id);
    }

    @RequestMapping(value = "/matchFacial", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> compareFaces(@RequestBody String jsonStringRequest) 
    {
        jsonStringRequest = jsonStringRequest.replace("\\\\", "\\");
        JSONObject jsonObject = new JSONObject(jsonStringRequest);
    	log.info("INFO: BiometricController.compareFaces:"+ LogUtil.logJsonObject(jsonObject));
        String b64Face1 = jsonObject.optString("b64Face1");
        String b64Face2 = jsonObject.optString("b64Face2");
        Integer umbral = jsonObject.optInt("umbral");
        return mbssHandler.matchFacial(b64Face1, b64Face2, umbral);
    }
    
    //--------------------------------------
	@Transactional
	@ApiOperation(value = "Gets the status for the requested serial number", response = String.class)
	@RequestMapping(value = "/addTypeToCurp/{idClie}/{type}", method = RequestMethod.GET)
	public String addTypeToCurp(@PathVariable Long idClie, @PathVariable String type) {
		log.info("INFO.agregando tipo de identificacion a curp");
		try {
			BidClieCurp bidClieCurp =  bidCurpRepository.findTopByIdClie(idClie);			
			// TODO guardar tipo de identificacion
			bidClieCurp.setType(type);			
			bidCurpRepository.save(bidClieCurp);
			log.info("INFO>OK");
			return "OK";
		} catch (Exception e) {
			log.error("ERROR>FAILL");
			return "FAILL";
		}
	}

}
